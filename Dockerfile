from tomcat
maintainer Kat McIvor 

#copy target/scalatra-maven-prototype.war /usr/local/tomcat/webapps/scalatra-maven-prototype.war
copy target/scalatra-maven-prototype.war /usr/local/tomcat/webapps/hello-scalatra.war

expose 8080

CMD ["catalina.sh", "run"]
